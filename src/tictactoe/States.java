package tictactoe;
/**
 *
 * @author Oliver Stimmer
 */

import java.util.ArrayList;

public class States {
	public static int[] current_state;
	public static Boolean xTurn = true;
	public static Boolean gameOver = false;
	public static int[] initial_state = new int[]{0,0,0,0,0,0,0,0,0};

	//
	public static void make_move(int moveToPosition){
		if (current_state[moveToPosition]==0 && !gameOver){
			if(xTurn){
				current_state[moveToPosition]=1;
				xTurn = false;}
			else{
				current_state[moveToPosition]=2;
				xTurn = true;}
		}
	}
	//this is algorithm  chooses response to move
	public static int calculateMoveAlgorithm(int[] state)   {
		findForkPossibility(xTurn, current_state);
		//1 if can put to center
		if(state[4]==0){
			System.out.println("Decison 1");
			return 4;}
		//2 if I can win, I'll win
		ArrayList<int[]> myWinningStates = whatStateWillWin(xTurn,current_state);
		if(myWinningStates.size()>0){
			System.out.println("Decison 2");
			return compareStates(current_state, myWinningStates.get(0));
		}
		//3 if oppopnent can with next turn, it will block it
		ArrayList<int[]> opponentWinningStates = whatStateWillWin(!xTurn,current_state);
		if(opponentWinningStates.size()>0){
			System.out.println("Decison 3");
			return compareStates(current_state, opponentWinningStates.get(0));
		}
		//4 check if I can make fork
		int forkPosition = findForkPossibility(xTurn, current_state);
		if (forkPosition != 99){
			System.out.println("Decison 4");
			return forkPosition;}
		//5 Check special positions
		if(state[5]==state[7]&&state[5]!=0&&state[8]==0){
			System.out.println("Decison 5.1");
			return 8;}
		if(state[0]==state[8]
				&&state[0]!=0
				&&state[0]!=state[4]&&
				countElement(current_state, 0)==6&&
				state[3]==0)
		{System.out.println("Decison 5.2");
		return 3;}
		if(state[2] == state[6]
				&& state[6]!=0
				&&state[2]!=state[4]&&
				countElement(current_state, 0)==6&&
				state[3]==0){
			{System.out.println("Decison 5.3");
			return 3;
			}}
		//6 Check if less than 5 free elements are on the table,
		// check  there is move where is hope to win and choose it.
		if(countElement(current_state, 0)<5){
			int hope =checkHopeToWin(xTurn, current_state);
			//System.out.println("hope "+hope);
			if (hope!=99){
				System.out.println("Decison 7");
				return hope;}
		}

		//7 prefered positions
		int[] positions =new int[]{0,2,6,8,1,3,5,7};
		for(int pos : positions){
			if(current_state[pos]==0){
				System.out.println("Decison 8");
				return pos;}
		}
		System.out.println("Decison 9");
		return 4;
	}
	//this generates all the next possible states
	public static ArrayList<int[]> genNextPossibleStates(Boolean xTurn, int[] state){
		int whoseTurn;
		if (xTurn){whoseTurn = 1;}
		else {whoseTurn = 2;}
		int[] state2 = state.clone();
		ArrayList<int[]> resultList = new ArrayList<>();
		for (int i=0; i<state.length; i++){
			if (state[i]==0){
				state2[i] = whoseTurn;
				resultList.add(state2);
				state2 = state.clone();
			}
		}
		return resultList;
	}
	//This gives all the states that allow to win with next move
	public static ArrayList<int[]> whatStateWillWin(Boolean xTurn, int[] state){
		ArrayList<int[]> nextPossibleStates =
				genNextPossibleStates(xTurn, state);
		ArrayList<int[]> nextPossibleWinningStates = new ArrayList<>();
		for(int[] stateX: nextPossibleStates){
			if(checkWin(stateX)!= null) {
				nextPossibleWinningStates.add(stateX);
			}
		}
		return nextPossibleWinningStates;
	}
	// If state has winning position than returns it, otherwise returns null
	public static int[] checkWin(int[] state){
		int[][] needToBeSame = new int[][]
				{{0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},
				{2,5,8},{0,4,8},{6,7,8},{2,4,6}};
		int a;
		int b;
		int c;
		for (int[] winPosition : needToBeSame) {
			a = winPosition[0];
			b = winPosition[1];
			c = winPosition[2];
			if (state[a]==state[b]& state[b]==state[c]
					&state[c]==state[a]&state[a]!=0) {
				return winPosition;
			}
		}
		return null;
	}

	//This method returns first position where states differ, if they are same returns 99
	public static int compareStates(int[]state1, int[] state2){
		for(int i = 0; i < state1.length; i++ ){
			if(state1[i]!=state2[i]){
				return i;
			}}
		return 99;
	}
	// Looks for fork possibilities. If find a postion where can make fork,
	//then returns the postion, otherwise returns 99.
	public static int findForkPossibility(Boolean xTurn, int[] state){
		ArrayList<int[]> nextStates = genNextPossibleStates(xTurn, state);
		for(int i =0; i<nextStates.size(); i++){
			ArrayList<int[]> whatStateWillWin = whatStateWillWin(xTurn, nextStates.get(i));
			if(whatStateWillWin.size()>1){
				//System.out.println("There is fork opportunity " + xTurn);
				//System.out.println(Arrays.deepToString(whatStateWillWin.toArray()));
				int dif1 = compareStates(current_state, whatStateWillWin.get(0));
				int dif2 = compareStates(current_state, whatStateWillWin.get(1));
				if(dif1==dif2)
				{return dif1;}
			}
		}
		return 99;
	}
	//checks if there are still postion that allows to win
	public static int checkHopeToWin(Boolean x_turn, int[] state) {
		ArrayList<int[]> nextStates = genNextPossibleStates(xTurn, state);
		for(int i = 0; i < nextStates.size(); i++ ){
			// nextStates.get(i);
			ArrayList<int[]> myWinningStates = whatStateWillWin(xTurn,nextStates.get(i));
			if(myWinningStates.size()>0){
				return compareStates(current_state, myWinningStates.get(0));
			}
		}
		return 99;
	}
	//counts specific element in array
	public static int countElement(int[] array, int whatCount){
		int counter = 0;
		for (int element: array){
			if(element==whatCount){counter++;}
		}
		return counter;
	}
}