package tictactoe;
/**
 *
 * @author Oliver Stimmer
 */
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;

public class GUI {
	//creates image icons
	ImageIcon b0ImageIcon = chooseImageIcon(0, States.current_state);
	ImageIcon b1ImageIcon = chooseImageIcon(1, States.current_state);
	ImageIcon b2ImageIcon = chooseImageIcon(2, States.current_state);
	ImageIcon b3ImageIcon = chooseImageIcon(3, States.current_state);
	ImageIcon b4ImageIcon = chooseImageIcon(4, States.current_state);
	ImageIcon b5ImageIcon = chooseImageIcon(5, States.current_state);
	ImageIcon b6ImageIcon = chooseImageIcon(6, States.current_state);
	ImageIcon b7ImageIcon = chooseImageIcon(7, States.current_state);
	ImageIcon b8ImageIcon = chooseImageIcon(8, States.current_state);

	//creates buttons
	JButton b0 = new JButton(b0ImageIcon);
	JButton b1 = new JButton(b1ImageIcon);
	JButton b2 = new JButton(b2ImageIcon);
	JButton b3 = new JButton(b3ImageIcon);
	JButton b4 = new JButton(b4ImageIcon);
	JButton b5 = new JButton(b5ImageIcon);
	JButton b6 = new JButton(b6ImageIcon);
	JButton b7 = new JButton(b7ImageIcon);
	JButton b8 = new JButton(b8ImageIcon);
	JButton bNewGame = new JButton("New Game");
	JButton[] tableButtons = new JButton[]{b0, b1, b2, b3, b4, b5, b6, b7, b8};

	JCheckBox IStart = new JCheckBox("I will start the next game",true); 


	public GUI(){
		// create JRrame with correct parameters
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Tic-Tac-Toe");
		frame.setSize(300, 300);
		frame.setLocation(150, 150);

		//create JPanels
		JPanel panelField = new JPanel();
		panelField.setLayout(new GridLayout(3,3));
		JPanel panelAdditionals = new JPanel();
		panelAdditionals.setLayout(new GridLayout(3,2));

		// <editor-fold defaultstate="collapsed" desc="Button listeners">
		// Add button listeners
		b0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button 0");
				performMove(0);
				frame.repaint();          
			}
		});

		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button 1");
				performMove(1);
				frame.repaint();
			}
		});

		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button 2");
				performMove(2);
				frame.repaint();
			}
		});

		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button 3");
				performMove(3);
				frame.repaint();
			}
		});

		b4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button 4");
				performMove(4);
				frame.repaint();
			}
		});

		b5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button 5");
				performMove(5);
				frame.repaint();
			}
		});

		b6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button 6");
				performMove(6);
				frame.repaint();
			}
		});

		b7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button 7");
				performMove(7);
				frame.repaint();
			}
		});

		b8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button 8");
				performMove(8);
				frame.repaint();
			}
		});

		bNewGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("You clicked the button new game");
				States.current_state = States.initial_state.clone();
				States.gameOver = false;
				for(int i=0; i < tableButtons.length; i++){
					tableButtons[i].setIcon(chooseImageIcon(i, States.current_state));
					tableButtons[i].setBackground(null);
				}
				if(IStart.isSelected()){
					States.xTurn = true;
				}
				else{States.xTurn = false;}
				frame.repaint();
			}
		});

		IStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(IStart.isSelected()){
					System.out.println("Is Seleceted");
					States.initial_state = new int[] {0,0,0,0,0,0,0,0,0};
				}
				else{System.out.println("Is not Seleceted");
				States.initial_state = new int[] {0,0,0,0,1,0,0,0,0};}
			}
		});
		// </editor-fold>
		// sets button prefered sizes
		b1.setPreferredSize(new Dimension(60, 60));
		b2.setPreferredSize(new Dimension(60, 60));
		b3.setPreferredSize(new Dimension(60, 60));      

		//add elements to panel
		panelField.add(b0);
		panelField.add(b1);
		panelField.add(b2);
		panelField.add(b3);
		panelField.add(b4);
		panelField.add(b5);
		panelField.add(b6);
		panelField.add(b7);
		panelField.add(b8);
		panelAdditionals.add(bNewGame);
		panelAdditionals.add(IStart);

		//add panels to frame and set frame visible
		frame.add(panelField, BorderLayout.NORTH);
		frame.add(panelAdditionals, BorderLayout.SOUTH);
		frame.setVisible(true);
	}
	//Chooses correct image to button according to current state.
	private ImageIcon chooseImageIcon(int number, int[] state){
		BufferedImage buttonIconX = null;
		BufferedImage buttonIconO = null;
		URL urlPathX =  GUI.class.getResource("pictures/x.png");
		URL urlPathO =  GUI.class.getResource("pictures/o.png");
		try {
			buttonIconX = ImageIO.read(urlPathX);
			buttonIconO = ImageIO.read(urlPathO);
		} catch (IOException ex) {
			Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
		}

		Image imageX = buttonIconX.getScaledInstance(50, 50,
				java.awt.Image.SCALE_SMOOTH);
		Image imageO = buttonIconO.getScaledInstance(50, 50,
				java.awt.Image.SCALE_SMOOTH);

		ImageIcon xImageIcon = new ImageIcon(imageX);
		ImageIcon oImageIcon = new ImageIcon(imageO);
		ImageIcon emptyImageIcon = new ImageIcon();

		if(state[number] == 0){return emptyImageIcon;}
		if(state[number] == 1){return xImageIcon;}
		if(state[number] == 2){return oImageIcon;}
		System.out.println("Ei tohiks kunagi siia j�uda");
		return emptyImageIcon;
	} 
	// Performs the move and response in GUI
	private void performMove(int position){
		//make humen move
		if(States.current_state[position]==0){
			JButton button = tableButtons[position];
			States.make_move(position);
			button.setIcon(chooseImageIcon(position, States.current_state));
			checkWin();
			//computer response move
			position = States.calculateMoveAlgorithm(States.current_state);
			button = tableButtons[position];
			States.make_move(position);
			button.setIcon(chooseImageIcon(position, States.current_state));
			checkWin();}
	}
	//This highlights the buttons if the win occures
	private void checkWin(){
		int[] winningPositions = States.checkWin(States.current_state);
		if(winningPositions != null){
			States.gameOver = true;
			for (int i=0; i<winningPositions.length; i++){
				tableButtons[winningPositions[i]].setBackground(Color.yellow);
			}
		}
	}
}

